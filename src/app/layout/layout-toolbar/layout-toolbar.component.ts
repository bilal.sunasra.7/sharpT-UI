import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-toolbar',
  templateUrl: './layout-toolbar.component.html',
  styleUrls: ['./layout-toolbar.component.scss']
})
export class LayoutToolbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
