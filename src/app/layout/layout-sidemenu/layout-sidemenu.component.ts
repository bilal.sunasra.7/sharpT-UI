import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-layout-sidemenu',
  templateUrl: './layout-sidemenu.component.html',
  styleUrls: ['./layout-sidemenu.component.scss']
})
export class LayoutSidemenuComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
  );

  public showNav: boolean = true;

  constructor(private breakpointObserver: BreakpointObserver) {}

}
